<?php

$baseNumber  = 'ABCD';
$currentBase = 16;
$targetBase  = 4;

function toAnyBase($number, $targetBase) {
    $chars = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F'];
    $str = "";
    do {
	$mod = $number % $targetBase;
	$number = (int) ($number / $targetBase);
	$str = $chars[$mod] . $str;
    } while ($number);

    return $str;
}

function anyBaseToDecimal($number, $currentBase) {
    $chars = ['0' => 0, '1' => 1, '2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, 'A' => 10, 'B' => 11, 'C' => 12, 'D' => 13, 'E' => 14, 'F' => 15];
    $str = strrev($number);
    $len = strlen($str);

    $newNumber = 0;
    for ($i = 0; $i < $len; $i++) {
	$ind = $chars[$str[$i]];
	$newNumber += $ind * ($currentBase ** $i);
    }
    return $newNumber;
}

if ($currentBase !== 10) {
    $baseNumber = anyBaseToDecimal($baseNumber, $currentBase);
}

echo toAnyBase($baseNumber, $targetBase);