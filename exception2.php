<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

set_exception_handler("myExceptionHandler");

function myExceptionHandler() {
    echo "something happened";
}

class DivideByZeroException extends Exception {};

try {
//$stack = new SplStack();
//$stack->pop();
    $a = 10;
    $b = 1;
    
    if($b == 0)
	throw new DivideByZeroException("sorry , no division by 0");
    
    $c = $a/$b;
    
    
    try {
	 $a = 10;
    $b = 0;
    
    if($b == 0)
	throw new DivideByZeroException("sorry , no division by 0");
    
    $c = $a/$b;
	
    }  finally {
	//echo "i am inside the nested finally";
    }
    
    echo "after handing the parent case";
    
    
} 
finally {
 echo "yes it is here";    
}

echo "done with exception handling";