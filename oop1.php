<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$name = "outside class";

class Programmer {
    var $name = "Programer";
    
    function sayName() {
	echo __CLASS__;
	$this->show();
    }
    
    function show() {
	//global $name;
	echo "my name is ".$this->name."<br />";
    }
}

class SEOMarketer {
    var $name = "SEO specialist";
    
    function sayName() {
	echo __CLASS__;
    }
}

$mizan = new Programmer();

$mizan->name = "mizanur rahman";

$hasin = new Programmer();

$hasin->name = "hasin hayder";

$sumon = new SEOMarketer();

print_r($mizan);
print_r($sumon);
$hasin->sayName();
$mizan->show();
$hasin->show();
$sumon->sayName();