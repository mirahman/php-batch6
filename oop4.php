<?php

class Programmer {
    var $name = "";
    var $lang = "";
    var $exp  = "";
    
    function __construct($name = "", $lang = "", $exp = "") {
	$this->name = $name;
	$this->lang = $lang;
	$this->exp = $exp;
    }
    
    function __destruct() {
	echo "i am destroying object<br />";
    }
    
    function sayName() {
	echo __CLASS__;
    }
    
    function __get($name) {
	echo "I do not have property $name<br />";
    }
    
    function __set($name, $value="") {
	echo "I can not set property $name<br />";
    }
    
    
    
    function __call($name, $arguments) {
	echo "No method named $name<br />";
    }
    
    function show() {
	//global $name;
	echo "my name is ".$this->name."<br />";
	echo "my fav lang is ".$this->lang."<br />";
	echo "i have experience of ".$this->exp." years<br />";
    }
}

$mizan = new Programmer("mizanur rahman","PHP, JS",10);
$hasin = new Programmer("hasin hayder","PHP, Go",14);

$abc = new Programmer;

$mizan->specialization = "test";

print_r($mizan);

$mizan->showSpecilization();

print_r($abc);

$abc = NULL;

echo serialize($hasin);



$hasin->show();
$mizan->show();