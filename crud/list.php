<?php
include "pdo.php";
include ("header.php");


$sql = "select count(id) as total from drivers";
$query = $dbh->query($sql);
$data = $query->fetch(PDO::FETCH_OBJ);

$totalDriver = $data->total;

echo "total is :".$totalDriver;
    
$sql = "select * from drivers";
// pagination
$perPage = 1;
$start = 0;

if(isset($_GET['start']) && $_GET['start'] > 0) {
    $start = $_GET['start']-1;
} 

$sql .=" limit ".$start.", ".$perPage;


$strLink = "";

if($start > 0) {
    
    $strLink .= "<a class='paging' href='list.php?start=1'> << </a>";
    $strLink .= "<a class='paging' href='list.php?start=".($start-$perPage+1)."'> < </a>";
    
}

for($i = 1;$i<=$totalDriver;$i+=$perPage) {
    
     $selected = "";
     
    if($i == $start+1) {
           $selected = "selected";
    }
    
    $strLink .= "<a class='paging ".$selected."' href='list.php?start=".$i."'>".$i."</a>";
}

if(($start+1) < $totalDriver && $totalDriver > $perPage) {
   
    $strLink .= "<a class='paging' href='list.php?start=".($start+$perPage+1)."'> > </a>";
    $strLink .= "<a class='paging' href='list.php?start=".($totalDriver-$perPage+1)."'> >> </a>";
}



$query = $dbh->query($sql);

?>

        

            <h1>Drivers</h1>
            <br />
            <span class='col-md-6'>
            <a href="create.php" class="btn btn-success">+ Add New Driver</a>
	    
	    <a href="download.php" class="btn btn-warning">Download Driver</a>
            </span>
            <span class='col-md-6'>
                <form method='get' action='search.php'>
                    <input type='text' name='search' /> <input type='submit' class='btn btn-success' value='Search'>
                    
                </form>
            </span>
            <Br />
            <br />
            <table class="table table-responsive table-bordered table-striped">
                <thead>
                    <tr>
                        <th>
                            #
                        </th>
                        <th>
                            Driver Name
                        </th>
                        <th>License #</th>
                        <th>Gender</th>
                        <th>DOB</th>
                        <th>Verified?</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    <?php
                        if($query)
                        {   $i = $start+1;
                            foreach($query as $row):
                             ?>
                    <tr>
                        <td><?php echo $i++; ?>
                        </td>
                        <td><?php echo $row['driverName']; ?>
                        </td>
                        <td><?php echo $row['licenseNumber']; ?>
                        </td>
                        <td><?php echo $row['dob']; ?>
                        </td>
                        <td><?php echo $row['gender']; ?>
                        </td>
                        <td><?php echo $row['verified']; ?>
                        </td>
                        <td>
                            <a href='update.php?id=<?php echo $row['id']; ?>' class='btn btn-sm btn-info'>Edit</a>
                            <a href='delete.php?id=<?php echo $row['id']; ?>' class='btn btn-sm btn-danger'>Delete</a>
                        </td>
                    </tr>
                    
                    
                             <?php
                                                               
                            endforeach;
                        }
                    ?>
                </tbody>
                
                <tfoot>
                    <tr>
                        <td colspan="7">
                            <?php echo $strLink?>
                        </td>
                    </tr>
                </tfoot>
            </table>

<?php
include ("footer.php");
?>