<?php
session_start();
session_regenerate_id();

include "pdo.php";


if($_POST) {
    
    if($_SESSION['token'] != $_POST['token'] || !isset($_POST['token'])) {
        echo "invalid request";
        exit;
    }
    
    
    try {
	
$dbh->beginTransaction();
	
    $sql = "INSERT INTO drivers "
        . " SET "
        ."driverName = ?,"
        . "driverPicture = ?,"
        . "licenseNumber = ?,"
        . "dob = ?,"
        . "gender = ?,"
        . "verified = ?";

$stmt = $dbh->prepare($sql);

$stmt->bindParam(1, $dName);
$stmt->bindParam(2, $dPic);
$stmt->bindParam(3, $license);
$stmt->bindParam(4, $dob);
$stmt->bindParam(5, $gender);
$stmt->bindParam(6, $verify);


$dName = $_POST['name'];
$dPic = $_FILES ? $_FILES['picture']['name'] : '';
$license = $_POST['license'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$verify = isset($_POST['verify'])  ? $_POST['verify'] : "No";

$stmt->execute();

$driverID = $dbh->lastInsertId(); 

$sql = "INSERT into taxis"
	. " set "
	."ownerID = '".$_POST['owner']."',"
	."driverID = '".$driverID."',"
	."taxiNumber = '".$_POST['taxinumber']."'";

$dbh->query($sql);

$newID = $dbh->errorCode(); 

if($newID > 0) 
    throw new Exception ();
    } catch (Exception $e) {
	echo "yes exception happened";
	$dbh->rollBack();
    } finally {
	if($dbh->inTransaction())
	$dbh->commit();
     }
    unset($_SESSION['token']);



if($_FILES) {
    move_uploaded_file($_FILES['picture']['tmp_name'], "./uploads/".$_FILES['picture']['name']);
}


header("location:list.php");
    
}

$code = sha1("Abscd".rand(100,10000).time());
$_SESSION['token'] = $code;





include ("header.php");
echo session_id();
//echo $code;
?>

<h1>Add New Driver</h1>

<form action="create.php" method="POST" enctype='multipart/form-data'>
  <div class="form-group">
    <label for="exampleInputEmail1">Driver Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter your name" name="name">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">License #</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="License #" name="license">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">DOB</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="dob">
  </div>
    
    <div class="form-group">
    <label for="exampleInputPassword1">Taxi number</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="taxinumber">
  </div>
    
    <div class="form-group">
    <label for="exampleInputPassword1">Owner</label>
    <select name="owner">
	<option value="1">Mizan</option>
    </select>
  </div>
    
    <div class="form-group">
    <label for="exampleInputPassword1">Gender</label>
    <input type="radio" class="" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="gender" value="Male" checked> Male
        <input type="radio" class="" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="gender" value="Female" > Female
  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Verified?</label>
    <input type="checkbox" class="" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="verify" value="1"> Yes
       
  </div>
  <div class="form-group">
    <label for="exampleInputFile">Driver's Picture</label>
    <input type="file" id="exampleInputFile" name='picture'>
    <p class="help-block">Only JPG/JPEG/PNG</p>
  </div>
    <input type="hidden" name="token" value="<?php echo $code?>" /> 
  <button type="submit" class="btn btn-success">Submit</button>
</form>

<?php
include ("footer.php");
?>

