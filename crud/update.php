<?php
include "pdo.php";


if($_POST) {
    
    $sql = "update drivers "
        . " SET "
        ."driverName = :dName,"
        . "driverPicture = :dPic,"
        . "licenseNumber = :license,"
        . "dob = :dob,"
        . "gender = :gender,"
        . "verified = :verified"
            . " where id = :id";

$stmt = $dbh->prepare($sql);

$stmt->bindParam(':dPic', $dPic);
$stmt->bindParam(':dName', $dName);
$stmt->bindParam(':license', $license);
$stmt->bindParam(':dob', $dob);
$stmt->bindParam(":gender", $gender);
$stmt->bindParam(":verified", $verify);
$stmt->bindParam(":id", $id);


$dName = strip_tags($_POST['name']); //htmlentities($_POST['name']);
$dPic = $_FILES ? $_FILES['picture']['name'] : $_POST['filename'];
$license = $_POST['license'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$verify = isset($_POST['verify'])  ? $_POST['verify'] : "No";

$id = $_POST['id'];

$stmt->execute();

//print_r($stmt->errorInfo());

if($_FILES) {
    move_uploaded_file($_FILES['picture']['tmp_name'], "./uploads/".$_FILES['picture']['name']);
}


header("location:list.php");
    
}
$driver = '';

if($_GET && $_GET['id'] > 0) {
    $query = $dbh->query("select * from drivers where id = '".$_GET['id']."'");
    
    $driver = $query->fetch(PDO::FETCH_OBJ);
    
}




include ("header.php");
?>

<h1>Update Driver</h1>
<?php if($driver): ?>
<form action="update.php" method="POST" enctype='multipart/form-data'>
  <div class="form-group">
    <label for="exampleInputEmail1">Driver Name</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter your name" name="name" value='<?php echo $driver->driverName?>'>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">License #</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="License #" name="license" value='<?php echo $driver->licenseNumber?>'>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">DOB</label>
    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="dob" value='<?php echo $driver->dob?>'>
  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Gender</label>
    <input type="radio" class="" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="gender" value="Male" <?php echo $driver->gender == "Male" ? 'checked' : '' ?>> Male
        <input type="radio" class="" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="gender" value="Female" <?php echo $driver->gender == "Female" ? 'checked' : '' ?> > Female
  </div>
    <div class="form-group">
    <label for="exampleInputPassword1">Verified?</label>
    <input type="checkbox" class="" id="exampleInputPassword1" placeholder="yyyy-mm-dd" name="verify" value="1" <?php echo $driver->verified == "1" ? 'checked' : '' ?>> Yes
       
  </div>
  <div class="form-group">
    <label for="exampleInputFile">Driver's Picture</label>
    <input type="file" id="exampleInputFile" name='picture'>
    <p class="help-block">Only JPG/JPEG/PNG</p>
    <input type='hidden' name='filename' value='<?php echo $driver->driverPicture?>' />
  </div>
    <input type="hidden" name='id' value='<?php echo $driver->id?>' />
  <button type="submit" class="btn btn-success">Submit</button>
</form>
<?php
else:
    echo "Sorry driver not found";
endif;

include ("footer.php");
?>

