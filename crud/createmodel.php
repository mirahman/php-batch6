<?php
session_start();
session_regenerate_id();

include "pdo.php";


if($_POST) {
    
    if($_SESSION['token'] != $_POST['token'] || !isset($_POST['token'])) {
        echo "invalid request";
        exit;
    }
    
    $sql = "INSERT INTO drivers "
        . " SET "
        ."driverName = ?,"
        . "driverPicture = ?,"
        . "licenseNumber = ?,"
        . "dob = ?,"
        . "gender = ?,"
        . "verified = ?";

$stmt = $dbh->prepare($sql);

$stmt->bindParam(1, $dName);
$stmt->bindParam(2, $dPic);
$stmt->bindParam(3, $license);
$stmt->bindParam(4, $dob);
$stmt->bindParam(5, $gender);
$stmt->bindParam(6, $verify);


$dName = $_POST['name'];
$dPic = $_FILES ? $_FILES['picture']['name'] : '';
$license = $_POST['license'];
$dob = $_POST['dob'];
$gender = $_POST['gender'];
$verify = isset($_POST['verify'])  ? $_POST['verify'] : "No";

$stmt->execute();

unset($_SESSION['token']);



if($_FILES) {
    move_uploaded_file($_FILES['picture']['tmp_name'], "./uploads/".$_FILES['picture']['name']);
}


header("location:list.php");
    
}


