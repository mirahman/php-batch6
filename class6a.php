<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$grade = "D";

switch($grade)
{
    case "A+":
    case "A":
	echo "Grade Point 4.0";
    break;


    case "A-":
	echo "Grade Point 3.7";
    break;

    case "B+":
	echo "Grade Point 3.3";
    break;

    case "B":
	echo "Grade Point 3.0";
    break;
    
    case "B-":
	echo "Grade Point 2.7";
    break;

    case "C+":
	echo "Grade Point 2.3";
    break;

    default:
	echo "Not a valid grade";
}


// wrong output
// we should not use comparison in case

$a = 0;


switch($a)
{
    case ($a>10 && $a<20):
        echo "i am good";
}